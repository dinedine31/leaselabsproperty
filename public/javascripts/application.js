$(document).ready(function() {

var setWidth = function() {
  $('#content-wrapper').css('width', '100%').css('width', '-=280px');
  $('body:not(#location) #main-content').css('padding-top', '0');
  $(window).on('resize', function() {
    $('#content-wrapper').css('width', '100%').css('width', '-=280px');
    $('#main-content').css('padding-top', '55px');
  });
}
  var mobileBreak = 640;
  var setHeight = function() {
    var wheight = $(window).height();
    if ($(window).width() > mobileBreak) {
      $('#secondary-nav').height(wheight);
    } else {
      $('#secondary-nav').css('height', 'auto');
    }
  }

  var setResize = function() {
    $(window).on('resize', function() {
      wheight = $(window).height();
      if ($(window).width() > mobileBreak) {
        $('#secondary-nav').height(wheight);
      } else {
        $('#secondary-nav').css('height', 'auto');
      }
    });
  }
  setHeight();
  setResize();
  setWidth();

  $('.fancybox').fancybox();

  // Hack to get off-canvas .menu-icon to fire on iOS
  $('.menu-icon').click(function() {
    false;
  });


  $('body#gallery a img').load(function() {
    $("#gallery-container").masonry();
  });

  var $container = $('#gallery-container');
  // initialize
  $container.masonry({
    gutter: 22,
    itemSelector: '.item'
  });


  $(function() {
    $('.photo-holder').maximage({
      cycleOptions: {
        fx: 'fade',
        speed: 1000,
        timeout: 5000,
        prev: '.bullets',
        next: '.bullets',
        pause: 1
      },
      fillElement: '.photo-block',
      backgroundSize: 'contain'
    });
  });



  // locations accordion
  $('body#location dl.accordion > dd:first-child').addClass('active');
  // $('.accordion > dd:first-child').addClass('active');

  // $('.accordion > dd').on('click', function() {
  //   $(this).addClass('active').siblings().removeClass('active');
  // });


  // locations tabs
  $('.tabs-content .content:first-child').addClass('active')
  $('.tabs-content > .content').on('click', function() {
    $(this).addClass('active').siblings().removeClass('active');
  });

  $('.tabs dd a:first-child').addClass('active')
  $('.tabs > dd a').on('click', function() {
    $(this).addClass('active').siblings().removeClass('active');
  });

  $('#main-content h1').addClass('title-border');

});

function GMapsSetOptions() {

  return {
    enable_property_marker_click: true,
    poi_link_activates_marker: true,
    activate_property_marker: true,
    activate_first_location: true,
    activate_first_poi: false,

    street_view_control: false,
    overview_control: false,
    rotate_control: false,
    scale_control: false,
    pan_control: false,
    type_control: {
      position: google.maps.ControlPosition.TOP_LEFT
    },
    zoom_control: {
      position: google.maps.ControlPosition.LEFT_TOP,
      style: google.maps.ZoomControlStyle.SMALL
    },
    map_zoom_range: [5, 14], // + INTEGER
    map_zoom_initial: 7, // + INTEGER
    mobile_breakpoint: 0, // + INTEGER 

    map_static_pin_src: 'markers/main-pin.png',

    poi_content: function(poi) {
      var has_full_address = (poi.address && poi.city && poi.state && poi.zip);

      var html_str = '';

      html_str += '<div class="gmaps_info_window">';
      html_str += (poi.name) ? '<h4 class="name">' + poi.name + '</h4>' : '';
      html_str += (poi.description) ? '<p class="description">' + poi.description + '</p>' : '';
      html_str += (has_full_address) ? '<p class="location"><span class="address">' + poi.address + '</span><br /><span class="city">' + poi.city + '</span><span>,&nbsp;</span><span class="state">' + poi.state + '</span><span>&nbsp;</span><span class="zip">' + poi.zip + '</span></p>' : '';
      html_str += (has_full_address || poi.href) ? '<p class="cta">' : '';
      html_str += (poi.href) ? '<a href="' + poi.href + '" title="' + poi.name + '" target="_blank">Visit Website</a><span>&nbsp;</span>' : '';
      html_str += (has_full_address) ? '<a href="http://maps.google.com/maps?daddr=' + poi.address + ',' + poi.city + ',' + poi.state + ',' + poi.zip + '" title="' + poi.name + '" target="_blank">Get Directions</a>' : '';
      html_str += (has_full_address || poi.href) ? '</p>' : '';
      html_str += '</div>';

      return html_str;
    }
  };
}